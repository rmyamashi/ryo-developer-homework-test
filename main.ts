import {
    GetProductsForIngredient,
    GetRecipes
} from "./supporting-files/data-access";
// DEV NOTE: Upon my implementation I am not able to find a use for the Product Model, but needed SupplierProduct & Recipe Model Instead.
// import {NutrientFact, Product, Recipe} from "./supporting-files/models";
import {NutrientFact, Recipe, SupplierProduct} from "./supporting-files/models";
import {
    GetCostPerBaseUnit,
    GetNutrientFactInBaseUnits
} from "./supporting-files/helpers";
import {RunTest, ExpectedRecipeSummary} from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

// Loop through the recipes and calculate the cheapest cost for each one
for (const recipe of recipeData) {
    const result = GetRecipeIngredientCosts(recipe);
    recipeSummary[recipe.recipeName] = result[recipe.recipeName];

  }

//  The RecipeSummary interface defines the structure of the summary object for each recipe,
//  including the cheapest cost and the nutrients at the cheapest cost.
interface RecipeSummary {
    [recipeName: string]: {
      cheapestCost: number;
      nutrientsAtCheapestCost: { [nutrientName: string]: NutrientFact };
    };
  }

function GetRecipeIngredientCosts(recipe: Recipe): RecipeSummary {
    let assembleCheapestRecipes = [];
    let totalCost = 0;
    let cheapestSupplier: SupplierProduct | undefined;

    //  This loop goes through each ingredient in the recipe,
    //  finds the product with the lowest cost for each ingredient,
    //  and keeps track of the total cost by accumulating the costs of all ingredients.
    for (const recipeIngredient of recipe.lineItems) {
        //  Dev Note: We're initializing cheapestCost to Infinity to ensure that the first cost value is always less than the initial value.
        //  Also, We're returning Infinity if there are no products available.
        let holdCost = Infinity;
        let cheapestProductNutrientFacts: NutrientFact[] = [];

        //  fetch the products for the ingredients used in the recipe.
        const fetchedProducts = GetProductsForIngredient(recipeIngredient.ingredient);

        //  Identify the supplier product that offers the best value in terms of cost for each ingredient,
        //  ensuring that the final recipe includes the cheapest options available.
        for (const singleProduct of fetchedProducts) {
            for (const fetchedSupplierProduct of singleProduct.supplierProducts) {
                const costPerBaseUnit = GetCostPerBaseUnit(fetchedSupplierProduct);
                if (costPerBaseUnit < holdCost) {
                    holdCost = costPerBaseUnit;
                    cheapestSupplier = fetchedSupplierProduct;
                    cheapestProductNutrientFacts = singleProduct.nutrientFacts.map(nutrientFact => GetNutrientFactInBaseUnits(nutrientFact));
                }
            }
        }
        //  Push objects representing the ingredient and its information into the assembleCheapestRecipes array.
        assembleCheapestRecipes.push({
            ingredientName: recipeIngredient.ingredient.ingredientName,
            supplierProduct: cheapestSupplier,
            nutrientFacts: cheapestProductNutrientFacts,
            unitOfMeasure: recipeIngredient.unitOfMeasure
        });

        //  Calculates the cost by multiplying the lowest cost (holdCost) by the amount of the unit of measure for the ingredient (recipeIngredient.unitOfMeasure.uomAmount).
        //  The result is added to the totalCost variable, accumulating the costs of all ingredients processed so far in the loop.
        totalCost += holdCost * recipeIngredient.unitOfMeasure.uomAmount;
    }

    //  Calculates the combined nutrient amounts for each nutrient at the cheapest cost across all the ingredients in the recipe.
    //  It aggregates the nutrient facts into a single object, nutrientsAtCheapestCost,
    //  where each key represents a nutrient name, and the associated value is the cumulative nutrient information for that nutrient from the cheapest products of each ingredient.
    const nutrientsAtCheapestCost: { [nutrientName: string]: NutrientFact } = {};

    for (const { nutrientFacts } of assembleCheapestRecipes) {
        for (const nutrientFact of nutrientFacts) {
            const nutrientName = nutrientFact.nutrientName;
            if (!nutrientsAtCheapestCost[nutrientName]) {
                nutrientsAtCheapestCost[nutrientName] = nutrientFact;
            } else {
                nutrientsAtCheapestCost[nutrientName].quantityAmount.uomAmount += nutrientFact.quantityAmount.uomAmount;
            }
        }
    }

    //  Extract the values from the nutrientsAtCheapestCost object and
    //  sorting them based on the quantityAmount.uomAmount property in descending order.
    const sortedNutrients = Object.values(nutrientsAtCheapestCost).sort(
        (a, b) => b.quantityAmount.uomAmount - a.quantityAmount.uomAmount
    );

    //  Creates the summary of the recipe's information using the calculated cheapest products found for each ingredients.
    //  It includes the recipe's name, the cheapest cost (which is the total cost of the ingredients),
    //  and the nutrients present in the recipe along with their quantities.
    //  The nutrients are stored in an object where the nutrient name is the key and the nutrient information is the value.
    const recipeSummary: RecipeSummary = {
        [recipe.recipeName]: {
            cheapestCost: cheapestSupplier ? totalCost : 0,
            //  Using reduce allows us to iterate over the sortedNutrients array and build an object with the nutrient names as keys and their corresponding nutrient information as values.
            nutrientsAtCheapestCost: sortedNutrients.reduce((acc, nutrient) => {
                acc[nutrient.nutrientName] = nutrient;
                return acc;
            }, {}),
        },
    };

    return recipeSummary;
  };

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
